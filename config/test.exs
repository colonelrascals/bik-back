use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :bik_back, BikBackWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Decrease the security around passwords for testing ONLY
config :bcrypt_elixir, :log_rounds, 4

# Configure your database
config :bik_back, BikBack.Repo,
  username: "patrick.hildreth",
  password: "",
  database: "bik_back_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
