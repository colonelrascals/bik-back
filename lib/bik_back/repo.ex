defmodule BikBack.Repo do
  use Ecto.Repo,
    otp_app: :bik_back,
    adapter: Ecto.Adapters.Postgres
end
